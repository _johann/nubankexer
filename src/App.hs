{-# LANGUAGE OverloadedStrings #-}
module App (mainTest, main) where

import           Control.Monad.IO.Class
import           Control.Monad.Reader                 (ReaderT, runReaderT, unless)
import           Data.Aeson                           (FromJSON, ToJSON,
                                                       Value (Null), object,
                                                       (.=))
import           Data.Default                         (def)
import           Data.Maybe
import           Data.Text.Lazy                       (Text)
import           Data.Time
import           Data.Time.Clock                      (getCurrentTime)
import qualified Database.Persist                     as DB
import qualified Database.Persist.Sqlite              as DB
import           GHC.Generics
import           Models.PersistentModels
import           Models.TxKind
import           Network.HTTP.Types.Status            (created201,
                                                       internalServerError500,
                                                       notAcceptable406,
                                                       expectationFailed417,
                                                       notFound404)
import           Network.Wai                          (Application, Middleware)
import           Network.Wai.Middleware.RequestLogger (logStdout, logStdoutDev)
import           Prelude
import           System.Environment                   (lookupEnv)
import           Logic
import           Web.Scotty.Trans                     (ActionT, Options,
                                                       ScottyT, defaultHandler,
                                                       delete, get, json,
                                                       jsonData, middleware,
                                                       notFound, param, post,
                                                       put, scottyAppT,
                                                       scottyOptsT, raise, settings,
                                                       showError, status, text,
                                                       verbose)

type Action = ActionT Text ConfigM ()

loggingM :: Environment -> Middleware
loggingM Development = logStdoutDev
loggingM Production  = logStdout
loggingM Test        = id

defaultH :: Environment -> Text -> Action
defaultH e x = do
  status internalServerError500
  let o = case e of
        Development -> object ["error" .= showError x]
        Production  -> Null
        Test        -> object ["error" .= showError x]
  json o

makeAccount :: Action
makeAccount = do
  accountDetails <- jsonData :: ActionT Text ConfigM Account
  tid <- runDB (DB.insert accountDetails)
  status created201
  json (DB.Entity tid accountDetails)

getDebtPeriod :: Action
getDebtPeriod = do
  accountID <- param "id"
  now <- liftIO getCurrentTime
  txns <- runDB $ txnsSince now (DB.toSqlKey (read accountID))
  let txns' = fmap DB.entityVal txns
      debtPeriods = mkDebtPeriod txns'
  json debtPeriods

getStatement :: Action
getStatement = do
  accountID <- param "id"
  now <- liftIO getCurrentTime
  txns <- runDB $ txnsSince now (DB.toSqlKey (read accountID))
  let txns' = fmap DB.entityVal txns
      statement = mkStatement txns'
  json statement

getBalance :: Action
getBalance = do
  accountID <- param "id"
  now <- liftIO getCurrentTime
  txns <- runDB $ txnsSince now (DB.toSqlKey (read accountID))
  let txns' = fmap DB.entityVal txns
      balance = mkSimpleBalance txns'
  json $ balance


handleTxnError :: Action
handleTxnError = do
  let err = "Amount must be a positive number greater than zero." :: String
      errKey = "error"
  (json (object [ errKey .= err ]))
  (status expectationFailed417)

storeTxn :: Txn ->
            Action
storeTxn txn = do
         runDB (DB.insert txn)
         status created201
         json txn

mkTxn :: Action
mkTxn = do
  txn <- jsonData :: ActionT Text ConfigM Txn

  if ((txnAmount txn) <= 0)
  then handleTxnError
  else storeTxn txn

getEnvironment :: IO Environment
getEnvironment = fmap (maybe Development read) (lookupEnv "SCOTTY_ENV")

getPort :: IO (Maybe Int)
getPort = do
  m <- lookupEnv "PORT"
  let p = case m of
            Nothing -> Nothing
            Just s  -> Just (read s)
  return p

getOptions :: Environment -> IO Options
getOptions e = do
  return def
    {  verbose = case e of
        Development -> 1
        _           -> 0
    }

getConfig :: IO Config
getConfig = do
  e <- getEnvironment
  p <- getPool e
  return Config
    { environment = e
    , pool = p
    }

application :: Config -> ScottyT Text ConfigM ()
application c = do
  let e = environment c
  defaultHandler (defaultH e)
  middleware (loggingM e)
  post "/account" makeAccount
  post "/account/transaction" mkTxn
  get "/account/:id/balance" getBalance
  get "/account/:id/debtperiod" getDebtPeriod
  get "/account/:id/statement" getStatement

runApplication :: Config -> IO Application
runApplication c = do
  o <- getOptions (environment c)
  let r m = runReaderT (runConfigM m) c
      app = application c
  putStrLn "Starting Server..."
  scottyAppT r app

migrateSchema :: Config -> IO ()
migrateSchema c =
  liftIO $ flip DB.runSqlPersistMPool (pool c) $ DB.runMigration migrateAll

mainTest :: IO Application
mainTest = do
  c <- getConfig
  migrateSchema c
  runApplication c

runApplication2 :: Config -> IO ()
runApplication2 c = do
  o <- getOptions (environment c)
  let r m = runReaderT (runConfigM m) c
      app = application c
  putStrLn "Starting Server..."
  scottyOptsT o r app

main :: IO ()
main = do
  c <- getConfig
  migrateSchema c
  runApplication2 c
