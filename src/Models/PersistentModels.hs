{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module Models.PersistentModels where

import           Control.Monad.IO.Class    (MonadIO, liftIO)
import           Control.Monad.Logger      (runNoLoggingT, runStdoutLoggingT)
import           Control.Monad.Reader      (MonadReader, ReaderT, asks,
                                            runReaderT)
import           Control.Monad.Trans.Class (MonadTrans, lift)
import           Data.Text                 (Text, pack)
import           Data.Time                 (UTCTime)
import           Database.Persist
import           Database.Persist.Sqlite
import           Database.Persist.TH
import           Database.Persist.Types    (PersistValue (PersistInt64))
import           GHC.Generics              (Generic)
import           Models.TxKind
import           System.Environment        (lookupEnv)

type ConnectionString = Text

data Config = Config
   { environment :: Environment
   , pool        :: ConnectionPool
   }

data Environment
  = Development
  | Production
  | Test
  deriving (Eq, Read, Show)

newtype ConfigM a = ConfigM
  { runConfigM :: ReaderT Config IO a
  } deriving (Applicative, Functor, Monad, MonadIO, MonadReader Config)

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|

Txn json
     kind              TxKind
     amount            Double
     time              UTCTime
     accountId         AccountId
     description       Text
     deriving Show Generic Eq

Account json
      name String
      deriving Show Generic Eq
|]

getDefaultConnectionString :: Environment ->
                              ConnectionString
getDefaultConnectionString e =
  let n = case e of
        Development -> "test.db"
        Production  ->  "prod.db"
        Test        -> ":memory:"
  in pack n

getConnectionString :: Environment ->
                       IO ConnectionString
getConnectionString e = do
  m <- lookupEnv "DATABASE_NAME"
  let s = case m of
        Nothing -> getDefaultConnectionString e
        Just u  -> pack u
  return s

getPool :: Environment ->
           IO ConnectionPool
getPool e = do
  s <- getConnectionString e
  runStdoutLoggingT $ createSqlitePool s 8

runDB :: (MonadTrans t, MonadIO (t ConfigM)) =>
         SqlPersistT IO a ->
         t ConfigM a

runDB q = do
  p <- lift (asks pool)
  liftIO (runSqlPool q p)

txnsSince :: UTCTime -> Key Account -> SqlPersistT IO [Entity Txn]
txnsSince time accountID = selectList
                           [TxnAccountId ==. accountID,
                            TxnTime <. time] [Asc TxnTime]
