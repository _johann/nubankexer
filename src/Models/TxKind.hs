{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric              #-}
module Models.TxKind  where
import           GHC.Generics        (Generic)
import Database.Persist.TH
import Data.Aeson (Value (Null), (.=), FromJSON, ToJSON, encode, object)
import Prelude

data TxKind = Withdrawal | Deposit | Purchase deriving (Show, Read, Eq, Generic)

instance FromJSON TxKind
instance ToJSON TxKind

derivePersistField "TxKind"
