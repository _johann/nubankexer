{-# LANGUAGE DeriveGeneric #-}
module Logic where

import Data.Aeson (FromJSON, ToJSON)
import qualified Data.Function as DF
import qualified Data.List as DL
import Data.Maybe
import Data.Time
import GHC.Generics
import Models.PersistentModels
import Models.TxKind

data SimpleBalance = SimpleBalance
               { amount :: Double}
             deriving (Eq, Show, Generic)

data Statement = Statement
                 { day :: Day,
                   balance :: Double,
                   transactions :: [Txn] }
               deriving (Show, Generic, Eq)

data AccountStatus = NoDebt | DebtStart |
                     DebtEnd | DebtCont
                   deriving (Show, Generic, Eq)

data DebtPeriod = DebtPeriod
                  { start :: Day,
                    end :: Maybe Day,
                    principle :: Double }
                deriving (Show, Generic, Eq)

instance ToJSON SimpleBalance
instance ToJSON Statement
instance ToJSON DebtPeriod
instance ToJSON AccountStatus

txnDay :: Txn -> Day
txnDay = utctDay . txnTime

mkNewBalance :: Txn -> Double -> Double
mkNewBalance (Txn Purchase amnt _ _ _) bal = bal - amnt
mkNewBalance (Txn Withdrawal amnt _ _ _) bal = bal - amnt
mkNewBalance (Txn Deposit amnt _ _ _) bal = bal + amnt

mkDebtPeriod' :: [Txn] -> [((AccountStatus, Double), Maybe DebtPeriod)]
mkDebtPeriod' txns = amount
  where amount = Prelude.foldl txnToDebtPeriod [] txns
        txnToDebtPeriod :: [((AccountStatus, Double), Maybe DebtPeriod)]
                        -> Txn
                        -> [((AccountStatus, Double), Maybe DebtPeriod)]
        -- Begin the comprehension with no DebtPeriod and a NoDebt status
        -- against a zero balance and the earliest transaction
        txnToDebtPeriod [] txn =
          (statusAndBal Nothing NoDebt 0 txn):[]
        -- When the comprehension step receives no DebtPeriod, we move
        -- on to the next transaction w/o appending to the accum
        txnToDebtPeriod (((accStat, bal), Nothing):xs) txn  =
          (statusAndBal Nothing accStat bal txn):xs
        -- When the comprehension step receives a DebtEnd account status,
        -- we append the steps results to the accumulation
        txnToDebtPeriod accum@(((DebtEnd, bal), debtP):xs) txn =
          ((statusAndBal debtP DebtEnd bal txn)):accum
        -- Otherwise the comprehension step iterates, building up new
        -- DebtPeriods that have not yet been resolved by an end date
        txnToDebtPeriod (((accStat, bal), debtP):xs) txn =
          ((statusAndBal debtP accStat bal txn)):xs
        statusAndBal :: Maybe DebtPeriod ->
                        AccountStatus ->
                        Double ->
                        Txn ->
                        ((AccountStatus, Double), Maybe DebtPeriod)
        statusAndBal debtP prevStatus bal txn =
          ((nStat, newBalance), updatedDebtP)
          where newBalance = mkNewBalance txn bal
                nStat = newStatus prevStatus newBalance
                updatedDebtP = mkDebtP debtP nStat
                -- NoDebt means we do not construct a new DebtPeriod
                mkDebtP _ NoDebt = Nothing
                -- DebtStart means we construct a new DebtPeriod beginning
                -- with the current action with an undefined endpoint and
                -- current amount of principle
                mkDebtP _ DebtStart =
                  Just $ DebtPeriod (txnDay txn) Nothing (abs newBalance)
                -- DebtCont means we update DebtPeriod's principle with
                -- latest balance
                mkDebtP (Just debtP) DebtCont =
                  Just $ debtP { principle =  abs newBalance }
                -- DebtEnd means we assign the current transactions day
                -- as the endpoint for the DebtPeriod
                mkDebtP (Just debtP) DebtEnd =
                  Just $ debtP { end = Just $ (addDays (-1) (txnDay txn))  }
                -- NoDebt maps to either a new period of debt, or a
                -- continuation
                newStatus NoDebt nBal
                  | nBal < 0 = DebtStart
                  | otherwise = NoDebt
                -- DebtStart transitions to either a continuation or an end
                newStatus DebtStart nBal
                  | nBal < 0 = DebtCont
                  | otherwise = DebtEnd
                -- DebtCont transitions to either continuation or an end
                newStatus DebtCont nBal
                  | nBal < 0 = DebtCont
                  | otherwise = DebtEnd
                -- DebtEnd transitions to either a continuation or a new debt period
                newStatus DebtEnd nBal
                  | nBal < 0 = DebtStart
                  | otherwise = NoDebt


mkDebtPeriod = catMaybes . (fmap snd) . mkDebtPeriod'

mkStatement :: [Txn] -> Maybe [Statement]
mkStatement [] = Nothing
mkStatement txns = statement
  where statement = Just $ Prelude.foldl foldTxn [] txns
        foldTxn :: [Statement] -> Txn -> [Statement]
        foldTxn [] txn = [(Statement (txnDay txn) (mkNewBalance txn 0) [txn])]
        foldTxn accum@((Statement day bal txns):xs) txn
          | day == (txnDay txn) = (Statement (txnDay txn) (mkNewBalance txn bal) (txn:txns)):xs
          | otherwise = (Statement (txnDay txn) (mkNewBalance txn bal) [txn]):accum

dayStatementToSimpleBalance = SimpleBalance . balance

mkSimpleBalance = (fmap convert) . mkStatement
  where convert = dayStatementToSimpleBalance . head
