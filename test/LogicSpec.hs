{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module LogicSpec (spec) where

import           App                       (main)
import           Control.Applicative
import           Data.Aeson                hiding (json)
import qualified Data.ByteString           as BS
import           Data.String
import           Data.Text                 (Text, pack)
import           Data.Time
import           Database.Persist.Sql      (toSqlKey)
import qualified Logic                     as L
import qualified Models.PersistentModels   as PM
import qualified Models.TxKind             as M
import           Test.Hspec                hiding (pending)

import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON
import           Test.Hspec.Wai.QuickCheck
import           Test.QuickCheck.Modifiers


txns = [(PM.Txn M.Deposit
         1000.00
         (UTCTime (fromGregorian 1970 10 15) (fromIntegral 10000))
          (toSqlKey 1)
          "What")
       , (PM.Txn M.Withdrawal
          3.34
          (UTCTime (fromGregorian 1970 10 16) (fromIntegral 10000))
          (toSqlKey 1)
          "Amazon")
       , (PM.Txn M.Withdrawal
          45.23 (UTCTime (fromGregorian 1970 10 16) (fromIntegral 19000))
          (toSqlKey 1)
          "Uber")
       ,  (PM.Txn M.Withdrawal
           180.00
           (UTCTime (fromGregorian 1970 10 17) (fromIntegral 10000))
           (toSqlKey 1)
           "What")]

statement = [(L.Statement
              (fromGregorian 1970 10 15)
              1000.00
              [(PM.Txn M.Deposit
                1000.00
                (UTCTime (fromGregorian 1970 10 15) (fromIntegral 10000))
                 (toSqlKey 1)
                 "What")])
            , (L.Statement
               (fromGregorian 1970 10 16)
               951.43
               [(PM.Txn M.Withdrawal
                 45.23
                 (UTCTime (fromGregorian 1970 10 16) (fromIntegral 19000))
                 (toSqlKey 1)
                 "Uber"),
                 (PM.Txn M.Withdrawal
                  3.34
                  (UTCTime (fromGregorian 1970 10 16) (fromIntegral 10000))
                   (toSqlKey 1)
                   "Amazon")])
            , (L.Statement
               (fromGregorian 1970 10 17)
               771.43
               [(PM.Txn M.Withdrawal
                 180.00
                 (UTCTime (fromGregorian 1970 10 17) (fromIntegral 10000))
                  (toSqlKey 1)
                  "What")])]

periodTxns = [(PM.Txn M.Deposit
                1000.00
                (UTCTime (fromGregorian 1970 10 15) (fromIntegral 10000))
                (toSqlKey 1)
                "What")
             , (PM.Txn M.Purchase
                 3.34
                 (UTCTime (fromGregorian 1970 10 16) (fromIntegral 10000))
                 (toSqlKey 1)
                 "Amazon")
             , (PM.Txn M.Purchase
                45.23
                (UTCTime (fromGregorian 1970 10 16) (fromIntegral 19000))
                (toSqlKey 1)
                "Uber")
             , (PM.Txn M.Withdrawal
                180.00
                (UTCTime (fromGregorian 1970 10 17) (fromIntegral 10000))
                  (toSqlKey 1)
                 "What")
             , (PM.Txn M.Purchase
               800.00
               (UTCTime (fromGregorian 1970 10 18) (fromIntegral 10000))
               (toSqlKey 1)
               "pardon")
             , (PM.Txn M.Deposit
                100.00
                (UTCTime (fromGregorian 1970 10 25) (fromIntegral 10000))
                (toSqlKey 1)
                "flight ticket")]

debtPeriod :: [L.DebtPeriod]
debtPeriod = [(L.DebtPeriod
               (fromGregorian 1970 10 18)
               (Just (fromGregorian 1970 10 24))
               28.57000000000005)]

spec :: Spec
spec = do
  describe "balance" $ do
    it "should calculate balance" $ do
      L.mkSimpleBalance txns `shouldBe` Just (L.SimpleBalance 771.43)
  describe "statement" $ do
    it "should calculate statement" $ do
      L.mkStatement txns `shouldBe` Just (reverse statement)
  describe "debt period" $ do
    it "should record debt period" $ do
      L.mkDebtPeriod periodTxns `shouldBe` debtPeriod
