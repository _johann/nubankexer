{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module ServerSpec (spec) where

import           App                       (mainTest)
import           Control.Applicative
import           Data.Aeson                hiding (json)
import qualified Data.ByteString           as BS
import           Data.String
import           Data.Text                 (Text, pack)
import           Data.Time
import           Database.Persist.Sql      (toSqlKey)
import qualified Models.PersistentModels   as PM
import qualified Models.TxKind             as M
import           Test.Hspec                hiding (pending)
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON
import           Test.Hspec.Wai.QuickCheck
import           Test.QuickCheck.Modifiers


instance Arbitrary M.TxKind where
  arbitrary = do
    n <- choose(0, 1) :: Gen Int
    let kind = case n of
                 0 -> M.Withdrawal
                 1 -> M.Deposit
    return kind

instance Arbitrary UTCTime where
    arbitrary =
        do randomDay <- choose (1, 29) :: Gen Int
           randomMonth <- choose (1, 12) :: Gen Int
           randomYear <- choose (2001, 2002) :: Gen Integer
           randomTime <- choose (0, 86401) :: Gen Int
           return $ UTCTime (fromGregorian randomYear randomMonth randomDay) (fromIntegral randomTime)

instance Arbitrary PM.Txn where
  arbitrary = do
    kind <- arbitrary
    (Positive amt) <-  arbitrary
    time <- arbitrary
    let actID = toSqlKey 1
        descr = "six thirty"
    return $ PM.Txn kind amt time actID descr

getAccount =
   describe "GET /account" $ do
    it "responds with HTTP status 200" $ do
      post "/account" [json|{"name": "Gerald Wallace"}|]  `shouldRespondWith` 201

txnAnyDateOrder = do
  property $ \(txn) ->
               do post "/account/transaction"
                       (encode (txn :: PM.Txn))  `shouldRespondWith` 201

rebukeAmtLessZero = do
                       post "/account/transaction" [json|{"kind": "Withdrawal", "amount":-3.51, "description": "america", "time": "2012-01-04T01:44:22.964Z", "accountId": 1}|]  `shouldRespondWith` 417

rebukeAmtZero = do
  post "/account/transaction" [json|{"kind": "Withdrawal", "amount":0, "description": "america", "time": "2012-01-04T01:44:22.964Z", "accountId": 1}|]  `shouldRespondWith` 417

postTxn =
  describe "POST /account/transaction" $ do
           it "can insert in any date order" txnAnyDateOrder
           it "does not accept transaction amounts less than zero" rebukeAmtLessZero
           it "does not accept transaction amount equal to zero" rebukeAmtZero

spec :: Spec
spec = with mainTest $ do
            getAccount
            postTxn
